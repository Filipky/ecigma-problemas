package aplicacion;



import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.io.*;

/**
 * The test class AutomataCelulcarTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class AutomataCelulcarTest
{
       AutomataCelular ac;
       Izquierdosa i1 , i2;
       Celula c1 ;
       Celula c2;
       Pobladora p1 , p2;
       CelulaNecia n1 , n2;
       FelipeMarcos fm1 , fm2;
       Conway cw1 , cw2 , cw3 , cw4;
       File f;
       File fa;
    /**
     * Default constructor for test class AutomataCelulcarTest
     */
    public AutomataCelulcarTest()
    {
       
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
     ac=new AutomataCelular();
     c1 = new Celula(ac,1,4);
     c2 = new Celula(ac,1,5);
     i1 = new Izquierdosa(ac , 3 , 5);
     i2 = new Izquierdosa(ac , 3 , 6);
     p1 = new Pobladora(ac ,6 , 4);
     p2 = new Pobladora(ac , 6 , 5);
     n1 = new CelulaNecia(ac  , 7 , 5);
     n2 = new CelulaNecia(ac , 7 , 6);
     fm1 = new FelipeMarcos(ac, 10 , 8);
     fm2 = new FelipeMarcos(ac , 15 ,9);
     cw1  = new Conway(ac , 5 , 1);
     cw2 =  new Conway(ac , 5 , 2);
     /*cw3 = new Conway(ac , 5  , 3);
     cw4 =  new Conway(ac ,   5  , 4);
     */
     f = new File("t1.dat");
     fa = new File("ta.dat");

       
       
    }
    
    @Test 
    public void deberiaCrearCelulasNuevas(){
       assertEquals(c1.edad() , 0);
       assertEquals(c2.edad() , 0);
       assertEquals(c2.getFila() , 1);
       assertEquals(c2.getColumna() , 5 );
       assertEquals(c1.getFila()  , 1) ;
       assertEquals(c1.getColumna() , 4);
       assertEquals(c1.isViva() , false);
       assertEquals(c2.isViva()  , false);
       
       assertEquals(i1.getFila() , 3);
       assertEquals(i1.getColumna() , 5);
       
       
      
    }

   @Test
   public void deberiaCambiarEnElPrimerTicCelulaNormal(){
        ac.ticTac();
        assertEquals(c1.edad() , 1  );
        assertEquals(c1.isViva()  , true);
        assertEquals(c2.isViva()  , true);
        assertEquals(c2.edad() ,  1 );
        
    }

    @Test
   public void deberiaCambiarEnElSegundoTicCelulaNormal(){
        ac.ticTac();
        ac.ticTac();
        assertEquals(c1.edad() , 2 );
        assertEquals(c1.isViva()  , true);
        assertEquals(c2.isViva()  , true);
        assertEquals(c2.edad() ,  2 );
        
    }
    
    @Test
   public void deberiaCambiarEnElTercerTicCelulaNormal(){
        ac.ticTac();
        ac.ticTac();
        ac.ticTac();
        assertEquals(c1.edad() , 3  );
        assertEquals(c1.isViva()  , false);
        assertEquals(c2.isViva()  , false);
        assertEquals(c2.edad() ,  3 );
        
        
    }
    
    @Test
    public void deberiaCambiaEnlosTresTicCelulaIzq(){
        ac.ticTac();
        assertEquals(i2.isViva()  , true);
        assertEquals(i1.isViva() , true);   
        ac.ticTac();
        assertEquals(i2.isViva()  , false);
        assertEquals(i1.isViva() , true); 
        ac.ticTac();
        assertEquals(i2.isViva()  , false);
        assertEquals(i1.isViva() , true); 
    }
    
    
    @Test 
    public void deberiaCambiarEnlosTresTicCelulaPobladora(){
        ac.ticTac();
        assertEquals(p1.isViva() , true);
        assertEquals(p2.isViva() , true);
        assertEquals(p2.edad() , 1);
        assertEquals(p1.edad() , 1);
        ac.ticTac();
        assertEquals(p1.isViva() , true);
        assertEquals(p2.isViva() , true);
        assertEquals(p2.edad() , 2);
        assertEquals(p1.edad() , 2);
        ac.ticTac();
        assertEquals(ac.getCelula(6,3) instanceof  Izquierdosa  , true );
        assertEquals(ac.getCelula(5,4) instanceof Pobladora, true);
        assertEquals(p1.isViva() , false);
        assertEquals(p2.isViva() , false);
        assertEquals(p2.edad() , 3);
        assertEquals(p1.edad() , 3);

        
        
    }
    
    @Test
    public void deberiaCambiarEnlosTresTicsCelulaNecia(){
        ac.ticTac();
        assertEquals(n1.edad() , 1  );
        assertEquals(n1.isViva()  , true);
        assertEquals(n2.isViva()  , true);
        assertEquals(n2.edad() ,  1 );
        ac.ticTac();
        assertEquals(n1.edad() , 2  );
        assertEquals(n1.isViva()  , true);
        assertEquals(n2.isViva()  , true);
        assertEquals(n2.edad() ,  2 );
        ac.ticTac();
        assertEquals(n1.edad() , 3  );
        assertEquals(n1.isViva()  , false);
        assertEquals(n2.isViva()  , false);
        assertEquals(n2.edad() , 3 );
             ac.ticTac();
        assertEquals(n1.edad() , 4  );
        assertEquals(n1.isViva()  , false);
        assertEquals(n2.isViva()  , false);
        assertEquals(n2.edad() , 4);
        
        
    }
    
    
    @Test 
    public void deberiaCambiarEnlosTresTicsCelulaFelipeMarcos(){
         ac.ticTac();
        assertEquals(fm1.edad() , 1  );
        assertEquals(fm1.isViva()  , true);
        assertEquals(fm2.isViva()  , true);
        assertEquals(fm2.edad() ,  1 );
        
        ac.ticTac();
        ac.ticTac();
        ac.ticTac();
        
        
        assertEquals(ac.getCelula(9,7) instanceof  Izquierdosa  , true );
        assertEquals(ac.getCelula(11,7) instanceof  CelulaNecia  , true );
        assertEquals(ac.getCelula(9,9) instanceof  Pobladora  , true );
        assertEquals(ac.getCelula(11,7) instanceof  Celula  , true );
        
    }
    
   @Test
    public void deberiaCambiarEnlosTresTicsCelulaConway(){
        assertEquals(cw1.isViva() , false);
        assertEquals(cw2.isViva() , false);
        ac.ticTac();
        assertEquals(cw1.edad() , 1  );
        assertEquals(cw2.isViva()  , true);
        assertEquals(cw1.isViva()  , true);
        assertEquals(cw2.edad() ,  1 );
        ac.ticTac();
        assertEquals(cw1.edad() , 2  );
        assertEquals(cw1.isViva()  , false);
        assertEquals(cw2.isViva()  , false);
        assertEquals(cw2.edad() ,  2 );
        
        
        
    }

    // Pruebas de Archivos 

    @Test
    public void debeGuardarArchivo (){
        try{

            AutomataArchivos.guarde(f ,  ac);
            assertEquals(true , f.exists());

        }catch(AutomataExcepcion e){
            fail("Error");
        }
    }


    @Test
    public void debeAbrir(){
        try{
        ac.ticTac();
        AutomataArchivos.guarde(fa , ac);
        
        AutomataCelular ac2  =  AutomataArchivos.abra(fa);

        }catch(AutomataExcepcion e){
            fail("Error");
        }
    }




    @Test
    public void debeExportarArchivo (){
        try{

            AutomataArchivos.exporte(f ,  ac);
            assertEquals(true , f.exists());

        }catch(AutomataExcepcion e){
            fail("Error");
        }
    }
    @Test
    public void debeImportarArchivo(){
        try{
        ac.ticTac();
        AutomataArchivos.exporte(fa , ac);
        
        AutomataCelular ac2  =  AutomataArchivos.importe(fa);

        }catch(AutomataExcepcion e){
            fail("Error");
        }
    }
    
     @Test(expected = AutomataExcepcion.class)
    public void noDeberiaEncontrarElArchivoA() throws AutomataExcepcion{
        ac.ticTac();
        AutomataArchivos.guarde(new File("z1.dat") , ac);
        AutomataCelular ac2  =  AutomataArchivos.abra(new File("z2.dat"));
    }
    @Test(expected = AutomataExcepcion.class)
    public void noDeberiaEncontrarElArchivoI() throws AutomataExcepcion{
        ac.ticTac();
        AutomataArchivos.exporte(new File("z1.dat") , ac);
        AutomataCelular ac2  =  AutomataArchivos.importe(new File("z2.dat"));
    }
    
    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }
}
