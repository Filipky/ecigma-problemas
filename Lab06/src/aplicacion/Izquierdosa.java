package aplicacion;
import java.awt.Color;

/**
 * Write a description of class Izquierdoza here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Izquierdosa extends Celula
{
    // instance variables - replace the example below with your own
    

    /**
     * Constructor for objects of class Izquierdoza
     */
    public Izquierdosa(AutomataCelular ac , int fila ,  int columna )
    {
        super(ac,fila,columna);
        color=Color.red;
        // initialise instance variables

    }

    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y   a sample parameter for a method
     * @return     the sum of x and y 
     */
    public void decida()
    {
        Celula celIzq = getAutomata().getCelula(getFila() , getColumna() - 1);
        
       //System.out.println("fil " + getFila()  +  "col" +  (getColumna() - 1));
       if( celIzq != null && celIzq.isViva() ) {
           estadoSiguiente = MUERTA;
          // System.out.println(" me mori");    
        }
       
      
    }
}
