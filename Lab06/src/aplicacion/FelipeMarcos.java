package aplicacion;
import java.awt.Color;

/**
 * Write a description of class FelipeMarcos here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class FelipeMarcos extends Celula
{

    /**
     * Constructor for objects of class FelipeMarcos
     */
    public FelipeMarcos(AutomataCelular ac,int fila, int columna)
    {
        super(ac , fila ,columna);
        color = Color.green;
    }

    public void decida(){
        
       if(edad() >= 3){ 
       Celula esi = getAutomata().getCelula(getFila() - 1 , getColumna() - 1);
       Celula esd = getAutomata().getCelula(getFila()  - 1, getColumna() + 1 );
       Celula eii = getAutomata().getCelula(getFila()  +  1 , getColumna() - 1);
       Celula eid = getAutomata().getCelula(getFila()   + 1, getColumna() + 1 );
        if( esi == null || !esi.isViva() ){
            getAutomata().setCelula( 
                getFila() - 1, 
                getColumna() - 1  , 
                new Izquierdosa(getAutomata() , getFila() - 1 , getColumna() - 1 ));
        }
         if( esd == null || !esd.isViva() ){
            getAutomata().setCelula( 
                getFila() - 1, 
                getColumna() + 1  , 
                new Pobladora(getAutomata() , getFila() - 1 , getColumna() + 1 ));
        }
        if( eii == null || !eii.isViva() ){
            getAutomata().setCelula( 
                getFila() + 1, 
                getColumna() - 1  , 
                new CelulaNecia(getAutomata() , getFila() + 1 , getColumna() - 1 ));
        }
         if( esd == null || !esd.isViva() ){
            getAutomata().setCelula( 
                getFila() + 1, 
                getColumna() + 1  , 
                new Pobladora(getAutomata() , getFila() + 1 , getColumna() + 1 ));
        }
    }
    if(edad()>= 7) estadoSiguiente = MUERTA;
        
    }
}
