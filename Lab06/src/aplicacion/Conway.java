package aplicacion;
import java.awt.Color;

/**
 * Write a description of class Conway here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Conway extends Celula
{
    // instance variables - replace the example below with your own
    

    /**
     * Constructor for objects of class Conway
     */
    public Conway(AutomataCelular ac,int fila, int columna) 
    {
        super(ac  , fila , columna);
        color = Color.blue;
    }

    /**
     * An example of a method - replace this comment with your own
     * 
     * @param  y   a sample parameter for a method
     * @return     the sum of x and y 
     */
   public void decida(){
       int  ad= 0;
       
       for(int i = getFila()- 1 ; i <  getFila() + 2  ; i++){
           for(int j = getColumna()  -  1  ; j < getColumna() + 2 ; j++){
                   if( i != getFila() || j !=  getColumna()){
                       if(getAutomata().getCelula(i , j) != null && getAutomata().getCelula(i , j).isViva() ){
                           ad+=1;
                           
                        }
                       
                    }
           }   
        }
        
        
 
    if(isViva()){   
        if(ad > 3 || ad == 1)estadoSiguiente = MUERTA;
    }else{
        if(ad == 3) estadoSiguiente = VIVA;
    }
    
    
   }
   
   
   public void nace(){
       int  ad= 0;
       
       for(int i = getFila()- 1 ; i <  getFila() + 2  ; i++){
           for(int j = getColumna()  -  1  ; j < getColumna() + 2 ; j++){
                   if( i != getFila() || j !=  getColumna()){
                       if(getAutomata().getCelula(i , j) == null){
                           checkParaNacer(i  , j);
                           
                        }
                       
                    }
           }   
        }
    }
    
    
    public void checkParaNacer( int  fil  ,  int col){
        int ad = 0;
        for(int i = fil ; i <  fil + 2  ; i++){
           for(int j = col  ; j < col + 2 ; j++){
                   if( i != getFila() || j !=  getColumna()){
                       if(getAutomata().getCelula(i , j) != null && getAutomata().getCelula(i , j).isViva() ){
                           ad+=1;
                        }
                       
                    }
           }   
        }
        if(ad >= 3){
            getAutomata().setCelula(fil , col , new Conway(getAutomata() , fil , col));
        }
        
        
    }
}
