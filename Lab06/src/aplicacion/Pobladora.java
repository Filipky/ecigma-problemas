package aplicacion;
import java.awt.Color;

/**
 * Write a description of class Pobladora here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Pobladora extends Celula
{

    /**
     * Constructor for objects of class Pobladora
     */
    public Pobladora(AutomataCelular ac , int fila ,  int columna )
    {
        // initialise instance variables
        super(ac,fila,columna);
        color=Color.orange;
    }

    /**
     * Decida
     */
    public void decida(){
       
       
       Celula izq = getAutomata().getCelula(getFila() , getColumna() - 1);
       Celula nor = getAutomata().getCelula(getFila()  - 1, getColumna() );
       if(edad() >= 2 && isViva()){
            if( izq == null || !izq.isViva() ){
                getAutomata().setCelula( getFila() , 
                getColumna() - 1  , 
                new Izquierdosa(getAutomata() , getFila() , getColumna() - 1 ));
            }
            if(nor == null || !nor.isViva()){
                getAutomata().setCelula( getFila() -1 , 
                getColumna() , 
                new Pobladora(getAutomata() , getFila()  - 1, getColumna() ));
            }
        }
       super.decida();
        
    }
}
