package aplicacion;

import java.util.*;
import java.io.*;


public class AutomataCelular implements Serializable{

	static private int LONGITUD=20;
	private Celula[][] celulas;

		
		
   public AutomataCelular() {
		celulas=new Celula[LONGITUD][LONGITUD];
		for (int f=0;f<LONGITUD;f++){
			for (int c=0;c<LONGITUD;c++){
				celulas[f][c]=null;
			}
		}
	}

	public int  getLongitud(){
		return LONGITUD;
	}

	public Celula getCelula(int f,int c){
		return celulas[f][c];
	}
	
	public void setCelula(int f, int c, Celula nueva){
		celulas[f][c]=nueva;
	}


   	public void ticTac(){
   	    for (int f=0;f<LONGITUD;f++){
			for (int c=0;c<LONGITUD;c++){
				if (celulas[f][c]!=null) celulas[f][c].decida();
			}
		}
		for (int f=0;f<LONGITUD;f++){
			for (int c=0;c<LONGITUD;c++){
	            if (celulas[f][c]!=null) celulas[f][c].cambie();
			}
		}
   	    
   	}
   	
  
}
