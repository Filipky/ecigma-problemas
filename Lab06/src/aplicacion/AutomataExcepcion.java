package aplicacion;
/**Excepciones de la aplicación
*/
public class AutomataExcepcion extends Exception{
	
	/**Crea una nueva excepción
	@param mensaje es el mensaje de la excepcion
	*/
	
	public AutomataExcepcion(String mensaje){
		super(mensaje);
	}
}