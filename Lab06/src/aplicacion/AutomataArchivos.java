package aplicacion;

import aplicacion.AutomataExcepcion;
import java.io.*;

public class AutomataArchivos{

	public static void guarde(File f, AutomataCelular a) throws AutomataExcepcion {
		//if (true) throw new AutomataExcepcion("Por construir");

		//if(a == null)	throw new AutomataExcepcion( AutomataExcepcion.NO_AUTOMATA );
		//if(f ==  null) throw new AutomataExcepcion(AutomataExcepcion.NO_ARCHIVO);
		try{
		ObjectOutputStream oos = new ObjectOutputStream( new FileOutputStream(f));
		oos.writeObject(a);
		oos.close();
		
		}catch(InterruptedIOException e){
		//	throw new AutomataExcepcion(AutomataExcepcion.NO_PERMISOS);
		}
		catch(IOException e){
			throw new AutomataExcepcion("Error");
		}	
		
	}

	public static AutomataCelular abra(File f)  throws AutomataExcepcion{
		
		AutomataCelular a =  null;
		//if(f ==  null) throw new AutomataExcepcion(AutomataExcepcion.NO_ARCHIVO);
		try{
			ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(f));
			a =  (AutomataCelular)ois.readObject();
			ois.close();

		}catch(FileNotFoundException e){
		//	throw new AutomataExcepcion(AutomataExcepcion.NO_ARCHIVO);
		}catch(InterruptedIOException e){
		//	throw new AutomataExcepcion(AutomataExcepcion.NO_PERMISOS);
		}
		catch(IOException e){
			throw new AutomataExcepcion("Error");
		}catch(ClassNotFoundException e){
			throw new AutomataExcepcion("Error");
		}
		return a;
	}
	
	
	public static void exporte(File f, AutomataCelular a) throws AutomataExcepcion{
		
		if(a == null)	throw new AutomataExcepcion(AutomataExcepcion.NO_AUTOMATA);
		if(f ==  null) throw new AutomataExcepcion(AutomataExcepcion.NO_ARCHIVO);
			try{
			PrintWriter bw = new PrintWriter(new FileOutputStream(f));
			for(int i = 0 ;  i < a.getLongitud()  ; i++){
				for(int j = 0 ; j < a.getLongitud(); j++){
					if( (a.getCelula( i , j) != null)){
						
						bw.println(a.getCelula( i , j ).getClass().getSimpleName()  + " " + i + " " + j );
					}
				}
			}

			bw.close();
		}
		catch(IOException e){
		
			throw new AutomataExcepcion("Error");
		}
		
	}



		public static AutomataCelular importe(File f) throws AutomataExcepcion{
		//	if(f ==  null) throw new AutomataExcepcion(AutomataExcepcion.NO_ARCHIVO);
			
		BufferedReader br;
		String line;
		AutomataCelular a = new AutomataCelular();
		Celula c;
		int i , j;
		try {
			br = new BufferedReader( new FileReader( f ) );
			while( (line = br.readLine() ) != null){

				String[] tmp = line.split(" ");
				i = Integer.parseInt(tmp[1]);
				j = Integer.parseInt(tmp[2]);

			
				if(tmp[0].equals("Celula")){
					c = new Celula( a , i , j);
				}else if(tmp[0].equals("Izquierdosa")){
					c = new Izquierdosa( a , i , j);
				}else if(tmp[0].equals("CelulaNecia")){
					c = new CelulaNecia(a , i , j);
				}else if(tmp[0].equals("Pobladora")){
					c = new Pobladora(a , i ,j);
				}else if(tmp[0].equals("FelipeMarcos")){
					c = new FelipeMarcos(a , i , j);
				}else if(tmp[0].equals("Conway")){
					c = new Conway(a  , i , j);
				}



			}
		}
		catch(InterruptedIOException e){
		//	throw new AutomataExcepcion(AutomataExcepcion.NO_PERMISOS);
		
		}catch(IOException e){
		
			throw new AutomataExcepcion("Error");
		}
		
		return a;
	}
}